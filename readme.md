To run this application:

* Install packages: `npm install`
   
* Run development server: `webpack-dev-server`

* Open application: `http://localhost:9000`