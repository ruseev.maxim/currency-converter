import {applyMiddleware, createStore} from 'redux';

import {fetchCurrency} from './actions/currency';

import createSagaMiddleware from 'redux-saga'


import rootSaga from './sagas/sagas';

const sagaMiddleware = createSagaMiddleware();

import createLogger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise-middleware';

import rootReducer from './reducers/index';

const middleware = applyMiddleware(sagaMiddleware, promise(), thunkMiddleware, createLogger);

const store = createStore(rootReducer, middleware);

sagaMiddleware.run(rootSaga);

export default store;


