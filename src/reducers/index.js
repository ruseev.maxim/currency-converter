import {combineReducers} from 'redux';

// Reducers
import currency from './currency';

const rootReducer = combineReducers({
  currency,
});

export default rootReducer;

