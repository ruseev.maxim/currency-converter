const currencyInitialState = {
  ui: {
    pending: false,
  },
  fromCurrencyName: '',
  fromCurrencyValue: '',
  toCurrencyName: '',
  toCurrencyValue: '',
  currenciesFulfilled: false,
  coefficients: {
    'USD/RUB': '',
    'RUB/USD': '',
    'EUR/RUB': '',
    'RUB/EUR': '',
  }
};

function currency(state = currencyInitialState, action) {
  let newState;
  switch(action.type) {

    case 'FETCH_CURRENCY_PENDING':
      return Object.assign({}, state, {ui: {pending: true}});

    //
    case 'FETCH_CURRENCY_FULFILLED':
      newState = {
        coefficients: {},
        currenciesFulfilled: true,
        ui: {pending: false},
      };

      ['USD/RUB', 'RUB/USD', 'EUR/RUB', 'RUB/EUR', 'EUR/USD', 'USD/EUR'].forEach((currencyName) => {
        newState.coefficients[currencyName] = action.payload.query.results.rate.find((rate) => rate.id === currencyName.replace('/', '')).Ask
      });

      if(state.fromCurrencyName && state.fromCurrencyValue && state.toCurrencyName) {
        newState.toCurrencyValue = state.fromCurrencyValue * newState.coefficients[`${state.fromCurrencyName}/${state.toCurrencyName}`];
      }

      return Object.assign({}, state, newState);
    case 'UPDATE_FROM_VALUE':
      newState = {
        fromCurrencyValue: action.fromValue
      };

      if(state.fromCurrencyName && state.toCurrencyName && action.fromValue && state.currenciesFulfilled) {
        newState.toCurrencyValue = action.fromValue * state.coefficients[`${state.fromCurrencyName}/${state.toCurrencyName}`];
      }
      return Object.assign({}, state, newState);

    case 'UPDATE_FROM_NAME':
      newState = {
        fromCurrencyName: action.fromName,
      };

      if(state.toCurrencyName && state.fromCurrencyValue && state.toCurrencyValue && action.fromName && state.currenciesFulfilled) {
        newState.toCurrencyValue = state.fromCurrencyValue * state.coefficients[`${action.fromName}/${state.toCurrencyName}`];
      }
      return Object.assign({}, state, newState);

    case 'UPDATE_TO_VALUE':
      newState = {
        toCurrencyValue: action.toValue,
      };

      if(state.fromCurrencyName && state.toCurrencyName && action.toValue && state.currenciesFulfilled) {
        newState.fromCurrencyValue = action.toValue * state.coefficients[`${state.toCurrencyName}/${state.fromCurrencyName}`];
      }
      return Object.assign({}, state, newState);

    case 'UPDATE_TO_NAME':
      newState = {
        toCurrencyName: action.toName,
      };

      if(state.fromCurrencyName && state.fromCurrencyValue && action.toName && state.currenciesFulfilled) {
        newState.toCurrencyValue = state.fromCurrencyValue * state.coefficients[`${state.fromCurrencyName}/${action.toName}`];
      }
      return Object.assign({}, state, newState);

    default:
      return state;
  }
}

export default currency;
