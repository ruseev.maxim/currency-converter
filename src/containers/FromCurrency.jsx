// react stuff
import React from 'react';
import {connect} from 'react-redux';

//actions
import {updateFromValue, updateFromName} from '../actions/currency';

// presentation components
import CurrencyInput from '../components/CurrencyInput';
import CurrencySelect from '../components/CurrencySelect';

// container
class FromCurrency extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <CurrencySelect
          value={this.props.fromCurrencyName}
          handleChange={this.props.handleSelectChange}
          disableCurrency={this.props.disableCurrency}
          label='Choose from currency'
        />
        <CurrencyInput
          value={this.props.fromCurrencyValue}
          onInput={this.props.updateFromValue}
          label='Enter from amount'
        />
      </div>
    )
  }
}

// map state to props
const mapStateToProps = (state) => {
  return {
    fromCurrencyName: state.currency.fromCurrencyName,
    fromCurrencyValue: state.currency.fromCurrencyValue,
    disableCurrency: state.currency.toCurrencyName,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateFromValue: (event) => dispatch(updateFromValue(event.target.value)),
    handleSelectChange: (event, index, value) => dispatch(updateFromName(value)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(FromCurrency);
