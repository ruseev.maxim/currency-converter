// react stuff
import React from 'react';
import {connect} from 'react-redux';

//actions
import {updateToValue, updateToName} from '../actions/currency';

// presentation components
import CurrencyInput from '../components/CurrencyInput';
import CurrencySelect from '../components/CurrencySelect';

// container
class ToCurrency extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <CurrencySelect
          value={this.props.toCurrencyName}
          handleChange={this.props.handleSelectChange}
          disableCurrency={this.props.disableCurrency}
          label='Choose to currency'
        />
        <CurrencyInput
          value={this.props.toCurrencyValue}
          onInput={this.props.updateToValue}
          label='Enter to amount'
        />
      </div>
    )
  }
}

// map state to props
const mapStateToProps = (state) => {
  return {
    toCurrencyName: state.currency.toCurrencyName,
    toCurrencyValue: state.currency.toCurrencyValue,
    disableCurrency: state.currency.fromCurrencyName,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateToValue: (event) => dispatch(updateToValue(event.target.value)),
    handleSelectChange: (event, index, value) => dispatch(updateToName(value)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ToCurrency);
