import axios from 'axios';
import {delay} from 'redux-saga'
import {put, takeEvery, all} from 'redux-saga/effects'

export function* pending() {
  console.log('heyy');
  yield delay(1000);
}

export function* watchPending() {
  yield put({type: 'FETCH_CURRENCY_PENDING'});
  try {
    const currencyApi = 'https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,RUBUSD,EURRUB,RUBEUR,EURUSD,USDEUR%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
    const response = yield axios.get(currencyApi);

    console.log(response);
    yield put({
      type: 'FETCH_CURRENCY_FULFILLED',
      payload: response.data,
    });
  } catch(e) {
    console.log(e);
    yield put({
      type: 'FETCH_CURRENCY_REJECTED',
      payload: {},
    })
  }

  yield takeEvery('FETCH_CURRENCY', pending);
}

export default function* rootSaga() {
  yield all([
    watchPending(),
  ])
}