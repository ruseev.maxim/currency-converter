const availableCurrencies = [
  {name: 'USD'},
  {name: 'RUB'},
  {name: 'EUR'},
];

export default availableCurrencies;