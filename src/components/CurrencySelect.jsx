// react stuff
import React from 'react';

// material ui
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import availableCurrencies from './AvailableCurrencies';

const CurrencySelect = ({value, handleChange, disableCurrency, label}) => {

  const currencies = availableCurrencies.map((currency) => {
    if(currency.name !== disableCurrency) {
      return <MenuItem
        key={currency.name}
        value={currency.name}
        primaryText={currency.name}
      />
    }

  });
  return (
    <div>
      <SelectField
        floatingLabelText={label}
        value={value}
        onChange={handleChange}
      >
        {currencies}
      </SelectField>

    </div>
  )
};

export default CurrencySelect;
