// react stuff
import React from 'react';
import {Link} from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Toolbar>
        <ToolbarGroup>
          <ToolbarTitle text="Currency converter" />
          <RaisedButton primary={true} label="Converter" containerElement={<Link to="/converter"/>}/>
          <RaisedButton primary={true} label="Info page" containerElement={<Link to="/info"/>}/>
        </ToolbarGroup>
      </Toolbar>

    )
  }
}

export default Navbar;