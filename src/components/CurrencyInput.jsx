// react stuff
import React from 'react';
import round from 'lodash/round';

// material ui
import TextField from 'material-ui/TextField';

const CurrencyInput = ({value, onInput, label}) => {
  if(value) value = round(value, 2);

  return (
    <TextField
      floatingLabelText={label}
      value={value}
      onInput={onInput}
    />
  )
};

export default CurrencyInput;
