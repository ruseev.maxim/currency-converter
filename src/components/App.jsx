// react stuff
import React from 'react';
import {connect} from 'react-redux';
import {Route} from 'react-router-dom';

//actions
import { fetchCurrency } from './../actions/currency';

// components
import Navbar from './Navbar';
import Converter from './Converter';
import Info from './Info';

import 'flexboxgrid';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchCurrency()
  }

  render() {
    if(!this.props.currenciesFulfilled) {
      return (
        <h2>Loading...</h2>
      )
    }
    return (
      <div>
        <div className="row">
          <div className="col-xs-12">
            <Navbar/>
          </div>

        </div>
        <Route path="/converter" component={Converter}/>
        <Route path="/info" component={Info}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    currenciesFulfilled: state.currency.currenciesFulfilled,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchCurrency: () => dispatch(fetchCurrency()),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
