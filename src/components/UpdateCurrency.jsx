// react stuff
import React from 'react'
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

// actions
import {fetchCurrency} from './../actions/currency';

// material ui
import RaisedButton from 'material-ui/RaisedButton';


class UpdateCurrency extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <RaisedButton
        onClick={this.props.handleClick}
        primary={true}
        disabled={this.props.updateCurrencyDisabled}
        label={this.props.currenciesFulfilled ? 'Update currencies' : 'Load currencies'}
      />
    )
  }
}

UpdateCurrency.propTypes = {
  updateCurrencyDisabled: PropTypes.bool,
};

const mapStateToProps = (state) => {
  return {
    updateCurrencyDisabled: state.currency.ui.pending,
    currenciesFulfilled: state.currency.currenciesFulfilled,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleClick: () => {
      dispatch(fetchCurrency());
    }
  }
};

UpdateCurrency = connect(mapStateToProps, mapDispatchToProps)(UpdateCurrency);


export default UpdateCurrency;
