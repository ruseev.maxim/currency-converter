import React from 'react';
import {connect} from 'react-redux';


const CurrentCurrency = ({currentCurrencyLabel}) => {
  return (
    <div className="current-currency">{currentCurrencyLabel}</div>
  )
};

const mapStateToProps = (state) => {
  let currentCurrencyLabel;

  // if currencies are not loaded
  if(!state.currency.currenciesFulfilled) {
    currentCurrencyLabel = 'Please click Load currencies';

  // if currencies are loaded, but from or to is not selected
  } else if(state.currency.currenciesFulfilled && (!state.currency.fromCurrencyName || !state.currency.toCurrencyName)) {
    currentCurrencyLabel = 'Select from and to currencies';

  // if currencies are loaded and from and to are selected
  } else if(state.currency.currenciesFulfilled && state.currency.fromCurrencyName && state.currency.toCurrencyName) {
    currentCurrencyLabel = `Your current currency is: ${state.currency.coefficients[`${state.currency.fromCurrencyName}/${state.currency.toCurrencyName}`]}`
  }
  return {
    currentCurrencyLabel: currentCurrencyLabel,
  }
};

export default connect(mapStateToProps)(CurrentCurrency);
