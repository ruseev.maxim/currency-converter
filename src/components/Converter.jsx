import React from 'react';

// components
import UpdateCurrency from './UpdateCurrency';
import CurrentCurrency from './CurrentCurrency';

// containers
import FromCurrency from './../containers/FromCurrency';
import ToCurrency from './../containers/ToCurrency';
const Converter = () => (
    <div className="row">
      <div className="col-xs">
        <FromCurrency/>
      </div>
      <div className="col-xs">
        <ToCurrency/>
      </div>
      <div className="col-xs">
        <CurrentCurrency/>
        <br/>
        <UpdateCurrency/>
      </div>
    </div>
  );


export default Converter;