import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import App from './App';

const Mui = () => (
  <MuiThemeProvider>
    <App/>
  </MuiThemeProvider>
);

export default Mui;