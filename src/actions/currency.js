import axios from 'axios';

const currencyApi = 'https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,RUBUSD,EURRUB,RUBEUR,EURUSD,USDEUR%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=';
function fetchCurrency() {
  return {
    type: 'FETCH_CURRENCY',
  }
  // return (dispatch) => {
  //   dispatch({
  //     type: 'FETCH_CURRENCY_PENDING'
  //   });
  //   axios.get(currencyApi).then((resp) => {
  //     dispatch({
  //       type: 'FETCH_CURRENCY_FULFILLED',
  //       payload: resp.data,
  //     })
  //   }).catch((err) => {
  //     dispatch({
  //       type: 'FETCH_CURRENCY_REJECTED',
  //       payload: err,
  //     })
  //   })
  // }
}

function updateFromValue(fromValue) {
  return {
    type: 'UPDATE_FROM_VALUE',
    fromValue,
  }
}

function updateFromName(fromName) {
  return {
    type: 'UPDATE_FROM_NAME',
    fromName,
  }
}

function updateToValue(toValue) {
  return {
    type: 'UPDATE_TO_VALUE',
    toValue,
  }
}

function updateToName(toName) {
  return {
    type: 'UPDATE_TO_NAME',
    toName,
  }
}

export {
  fetchCurrency,
  updateFromValue,
  updateFromName,
  updateToValue,
  updateToName
};
