const path = require('path');


module.exports = {
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  entry: './src/index.jsx',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'src'),
  },
  module: {
    rules: [{
      test: /\.css/,
      use: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader',
      }, {
        loader: 'sass-loader',
      }],
    }, {
      test: /\.jsx$/,
      exclude: [/node_modules/],
      use: [{
        loader: 'babel-loader',
        options: {presets: ['react', 'es2015']},
      }],
    }],
  },
  devtool: 'eval-source-map',
  devServer: {
    contentBase: path.join(__dirname, '/src'),
    historyApiFallback: true,
    compress: true,
    port: 9000,
  },
};
